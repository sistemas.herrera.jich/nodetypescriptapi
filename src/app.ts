import 'dotenv/config';
import server from './server';


const port = parseInt(process.env.PORT || '');

const started = new server().start(port)
    .then(port => console.log(`Runing on port ${port}`))
    .catch(err => console.error(err));

export default started;