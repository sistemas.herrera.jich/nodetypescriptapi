import express, { Application } from 'express';
import cors from 'cors';
import path from 'path';

import { 
    UsersRoutes, 
    AuthRoutes, 
    ProductosRoutes,
    ModeloRoutes,
    ClientesRoutes,
    PromocionesRoutes,
    ApartadoRoutes
} from './routes';

class Server {
    private app;
    
    constructor() {
        this.app = express();
        this.config();
        this.routeConfig();
    }

    private config(){
        this.app.use(cors());
        //cargar express
        this.app.use(express.json( {limit: '50mb'} ));
        this.app.use(express.urlencoded({ extended: true, limit: '50mb' }));
        // Static path public API
        const publicPath = path.join(__dirname, 'public');
        this.app.use(express.static(publicPath));
    }

    private routeConfig(){
        let usersRoutes = new UsersRoutes();
        let authRoutes = new AuthRoutes();
        let productosRoutes = new ProductosRoutes();
        let modeloRoutes = new ModeloRoutes();
        let clientesRoutes = new ClientesRoutes();
        let promocionesRoutes = new PromocionesRoutes();
        let apartado = new ApartadoRoutes();

        this.app.use('/users', usersRoutes.routes);
        this.app.use('/auth', authRoutes.routes);
        this.app.use('/productos', productosRoutes.routes);
        this.app.use('/modelo', modeloRoutes.routes);
        this.app.use('/clientes', clientesRoutes.routes);
        this.app.use('/promociones', promocionesRoutes.routes);
        this.app.use('/apartado', apartado.routes);
    }

    public start = (port: number) => {
        return new Promise((resolve, reject) => {
            this.app.listen(port, () => {
                resolve(port);
            }).on('error',(err: Object) => {
                reject(err);
            });
        });
    }
}

export default Server;