import { Router } from 'express';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
import { ApartadoController } from './apartado.controller';

const router = Router();

router.use(authenticated_JWT);

export class ApartadoRoutes{
    private apartado: ApartadoController;

    constructor(){
        this.apartado = new ApartadoController();
    }

    public get routes(){
        let controller = this.apartado;

        router.post('/create', controller.create);
        return router;
    }
}