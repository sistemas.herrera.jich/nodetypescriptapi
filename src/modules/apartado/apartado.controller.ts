import { Request, Response } from 'express';
import httpStatus from 'http-status';
import nodemailer from 'nodemailer';
import ApartadoService from './apartado.service';
import { EMAIL } from '../../helpers/emailsend';

let services = new ApartadoService();

export class ApartadoController {

    public async create(req: Request, res: Response){
        try {
            let obj = req.body;

            if (Object.entries(obj).length === 0) {
                return res.status(httpStatus.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se puede agregar objeto vacio"
                });
            }

            const insertId = await services.create(obj);
            const apartado = await services.getApartado(insertId[0].currval);
            const usuario = await services.getUsuario(apartado.id_user);
            const producto = await services.getProducto(apartado.codigo);

            const replacements = {
                vendedor: usuario.nombre,
                razonSocial: apartado.razon_social,
                domicilio: apartado.direccion,
                telefono: apartado.telefono,
                notes: apartado.notes,
                url: producto.url,
                codigo: producto.codigo,
                cantidad: apartado.piezas,
                productoName: producto.nombre,
                precio: apartado.precio
            };

            const htmlToSend = EMAIL(replacements);

            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                services:'gmail',
                auth: {
                    user: 'noreply.jhkmultiservicios@jhkmservicios.com',
                    pass: 'Jmu150331lx7',
                }
            });

            const info = await transporter.sendMail({
                from: apartado.tipo == "apartado"
                ? ' "Solicitud de apartado" <noreply.jhkmultiservicios@jhkmservicios.com>'
                : '"Cotización " <noreply.jhkmultiservicios@jhkmservicios.com>',
                to: apartado.tipo == "apartado"
                ? "hector.ceniceros@jhkmservicios.com"
                : apartado.email,
                cc: apartado.tipo == "apartado"
                ? "auxiliar@jhkmservicios.com, alexccp11@gmail.com"
                : "auxiliar@jhkmservicios.com, alexccp11@gmail.com, hector.ceniceros@jhkmservicios.com",
                subject: `"Pedido ${replacements.razonSocial}"`,
                text: `"Vendedor: ${replacements.vendedor}"`,
                html: htmlToSend,
            });

            res.status(httpStatus.OK).json({
                isValid: true,
                message: `Correo enviado`,
                data: info.response
            });

        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: 'Error al agregar apartado'
            });
        }
    }

    
}