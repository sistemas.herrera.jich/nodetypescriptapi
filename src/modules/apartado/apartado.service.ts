import pool from '../../database/config';
import { SQLUSER } from '../users/users.constant';
import { SQLPRODUCTOBYCODIGO } from '../productos/productos.constant';
import ConstantQuery from '../../global/constant-query';

const constantQuery = new ConstantQuery("apartados");

export default class ApartadosServices {
    private client;

    constructor(){
        this.client = pool;
    }

    public async create(apartado: any){
        try {
            if (Object.entries(apartado).length === 0) throw new Error("El objeto apartado esta vacio");

            let query = constantQuery.SQLINSERT(apartado);
            await this.client.query(query);
            let queryId = constantQuery.SQLINSERTID();
            const lastidrows = await this.client.query(queryId);
            return lastidrows.rows;
        } catch (error) {
            throw new Error("Error al crear apartado: "+error);
        }
    }

    public async getUsuario(id:number){
        try {
            let query = SQLUSER(id);
            const user = await this.client.query(query);
            return user.rows[0];
        } catch (error) {
            throw new Error("Error: "+error);
        }
    }

    public async getProducto(codigo: string){
        try {
            let query = `select productos.codigo as codigo, productos.nombre as nombre, 
                            productos.existencia as pieza, productos.id as id, modelo.url as url
                            from productos 
                            left join modelo on modelo.id = productos.id_modelo
                            where productos.codigo ='${codigo}'`;

            const producto = await this.client.query(query);
            return producto.rows[0];
        } catch (error) {
            throw new Error("Error: "+error);
        }
    }

    public async getApartado(id:number){
        try {
            let query = constantQuery.SQLGETBYONE('id', id);
            const apartado = await this.client.query(query);
            return apartado.rows[0];
        } catch (error) {
            throw new Error("Error: "+error);
        }
    }
}