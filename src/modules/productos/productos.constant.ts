export const SQLINSERT = (obj:any) => {
    let query:string[] = ['INSERT INTO productos'];
    let cols:string[] = [];
    Object.keys(obj).forEach(function(key, i){
        cols.push(key);
    });

    query.push(`( ${cols.join(', ')} ) VALUES`);

    let values:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        values.push(`'${obj[key]}'`);
    });

    query.push(`( ${values.join(', ')} )`);

    return query.join(' ');
}

export const SQLPRODUCTOBYCODIGO = (codigo:string) => {
    let query = `SELECT * from productos WHERE codigo != ${codigo}`;
    return query;
}

export const SQLUPDATE = (id:number, obj:any) => {
    let query:string[] = ['UPDATE productos SET'];
    let set:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        let item = `${key}='${obj[key]}'`;
        set.push(item);
    });
    query.push(set.join(', '));

    query.push(`WHERE id=${id}`);

    return query.join(' ');
}

export const SQLPRODUCTOS = (limit:number, page:number, filters:Object) => {
    let query = '';
    if (Object.entries(filters).length === 0) {
        query = `SELECT 
                    productos.*,
                    modelo.nombre as nombre_modelo,
                    modelo.url as imagen_producto,
                    sucursales.nombre as almacen
                FROM  productos
                LEFT JOIN modelo ON productos.id_modelo = modelo.id
                inner join sucursales on sucursales.idadminpaqalmacen = productos.idadminpaqalmacen 
                WHERE productos.existencia !=0
                ORDER BY productos.existencia DESC
                LIMIT ${limit}
                OFFSET ((${page} - 1 ) * ${limit})`;
    }else{
        query = filtersConstructor(filters, limit, page);
    }
     
    return query;
}

export const SQLROWSPRODUCTOSCOUNT = () => {
    let query = `SELECT count(id) from productos WHERE existencia != 0`;
    return query;
}

function filtersConstructor( filters:Object, limit:number, page:number ){
    let query :string[] = [`SELECT 
                                productos.*,
                                modelo.nombre as nombre_modelo,
                                modelo.url as imagen_producto,
                                sucursales.nombre as almacen
                            FROM  productos
                            LEFT JOIN modelo ON productos.id_modelo = modelo.id
                            inner join sucursales on sucursales.idadminpaqalmacen = productos.idadminpaqalmacen 
                            WHERE productos.existencia !=0`];
    let set:string[] = [];

    Object.keys(filters).forEach(function (key,i){
        let item = `AND productos.${key} LIKE '%${filters[key]}%'`;
        set.push(item);
    });
    query.push(set.join(' '));

    query.push(`ORDER BY productos.existencia LIMIT ${limit} OFFSET ((${page} - 1 ) * ${limit});`);

    return query.join(' ');
}