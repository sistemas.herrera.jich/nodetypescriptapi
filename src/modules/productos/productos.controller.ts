import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ProductoServices from './productos.service';

const service = new ProductoServices();

export default class ProductoController{

    public async getProductos(req: Request, res: Response){
        try {
            let limit = parseInt(req.params.limit);
            let page = parseInt(req.params.page);
            let filters = req.body;
            const productos = await service.getProductos(limit, page, filters);
            res.status(httpStatus.OK).json({
                isValid:true,
                message:"Listado de productos",
                data:productos
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error en la consulta de productos"
            });
        }
    }
}