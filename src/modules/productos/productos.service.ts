import pool from '../../database/config';
import {
    SQLPRODUCTOS,
    SQLROWSPRODUCTOSCOUNT
} from './productos.constant';
import ConstantQuery from '../../global/constant-query';

const constantQuery = new ConstantQuery("productos");

export default class ProductoServices {
    private client;

    constructor(){
        this.client = pool;
    }

    public async getProductos(limit:number = 8, page:number, filters:Object){
        try {
            let countRows = 0;

            let queryRows = constantQuery.SQLCOUNTROWS("id", "existencia",0, filters, "!=");
            const result = await this.client.query(queryRows);

            let query = SQLPRODUCTOS(limit, page, filters);
            const rows = await this.client.query(query);

            countRows = Math.ceil(result.rows[0].count/limit);

            let data = {
                page: page,
                limit: limit,
                numpages:countRows,
                data:rows.rows
            };
            return data;
        } catch (error) {
            throw new Error("Error al consultar productos: "+error);
        }
    }

}