import { Router } from 'express';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
import ProductoController from './productos.controller';
const router = Router();

router.use(authenticated_JWT);

export class ProductosRoutes{
    private productosController: ProductoController;

    constructor(){
        this.productosController = new ProductoController();
    }

    public get routes(){
        let controller = this.productosController;

        router.post('/all/:limit/:page', controller.getProductos);

        return router;
    }
}