import { Router } from 'express';
import UserController from './users.controller';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
const router = Router();

router.use(authenticated_JWT);

export class UsersRoutes {
    private usersController: UserController
    

    constructor() {
        this.usersController = new UserController()
    }

    public get routes(){
        let controller = this.usersController;

        router.post('/create', controller.createUsers);
        router.put('/update/:id', controller.updateUsers);
        router.post('/getAll/:limit/:page', controller.getUsers);
        router.get('/getUser/:id', controller.getUser);
        router.post('/addImage', controller.addImage);
        
        return router;
    }

}
