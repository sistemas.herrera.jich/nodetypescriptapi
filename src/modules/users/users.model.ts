export interface Users {
    id:number;
    nombre:string;
    apellidos:string;
    email:string;
    password:string;
    tipo:string;
}