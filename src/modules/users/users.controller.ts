import { Request, Response } from 'express';
import UsersServices from './users.service';
import httpStatus from 'http-status';

let services = new UsersServices();

class UserController {

    public async createUsers(req:Request, res: Response) {
        try {
            let obj = req.body;

            if (Object.entries(obj).length === 0) {
                return res.status(httpStatus.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se puede agregar objeto vacio"
                });
            }

            const message = await services.createUsers(obj);
            
            res.status(httpStatus.OK).json({
                isValid: true,
                message: message,
            });
        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: "Error no se pudo completar consulta"
            });
        }
    }

    public async updateUsers(req: Request, res: Response){
        try {
            let id = parseInt(req.params.id);
            let obj = req.body;
            const msg = await services.updateUsers(id,obj);
            res.status(httpStatus.OK).json({
                isValid: true,
                message:msg
            });
        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: "Error no se pudo completar consulta"
            });
        }
    }

    public async getUsers(req:Request, res:Response){
        try {
            const body = req.body;
            const limit = parseInt(req.params.limit);
            const page = parseInt(req.params.page);
            
            const users = await services.getUsers(limit, page, body);
            res.status(httpStatus.OK).json({
                isValid: true,
                message:'Listado de usuarios',
                data: users
            });
        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: "Error no se pudo completar consulta"
            });
        }
    }

    public async getUser(req: Request, res: Response){
        try {
            let id = parseInt(req.params.id);
            const user = await services.getUser(id);
            res.status(httpStatus.OK).json({
                isValid: true,
                message:'Usuario encontrado',
                data: user
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: "Error no se pudo completar consulta"
            })
        }
    }

    public async addImage(req: Request, res: Response){
        try {
            const { id, filename } = req.body;
            const data ={
                id,
                name:`image_${id}`,
                filename
            }

            const msg = await services.uploadImage(data);
            const user = await services.getUser(id);

            res.status(httpStatus.OK).json({
                isValid: true,
                message: msg,
                data:user
            });
            
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: "Error no se pudo subir la imagen" + error
            });
        }
    }
}

export default UserController;