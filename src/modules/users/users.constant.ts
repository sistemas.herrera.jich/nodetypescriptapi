import bcrypt from "bcryptjs";

export const SQLUSERS = () => {
    const query = `SELECT * FROM users WHERE status=1`;
    return query;
};

export const SQLUSER = (id:number) => {
    const query = `SELECT * FROM users WHERE status=1 AND id=${id}`;
    return query;
};

export const SQLUSERBYEMAIL = (email:string) => {
    const query = `SELECT id FROM users WHERE email='${email}'`;
    return query;
}

export const SQLUPDATEUSERS = (id:number, obj:any) => {
    let query:string[] = ['UPDATE users SET'];
    let set:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        if (key === 'email') {
            
        }else if(key === 'password') {
            let salt = bcrypt.genSaltSync();
            let password = bcrypt.hashSync(obj[key], salt);
            
            let item = `${key}='${password}'`;
            set.push(item);
        }else{
            let item = `${key}='${obj[key]}'`;
            set.push(item);
        }
    });
    query.push(set.join(', '));

    query.push(`WHERE id=${id}`);

    return query.join(' ');
}

export const SQLINSERTUSERS = (obj:any) => {
    let query:string[] = ['INSERT INTO users'];
    let cols:string[] = [];
    Object.keys(obj).forEach(function(key, i){
        cols.push(key);
    });

    query.push(`( ${cols.join(', ')} ) VALUES`);

    let values:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        if (key === 'password') {
            let salt = bcrypt.genSaltSync();
            let password = bcrypt.hashSync(obj[key], salt);
            values.push(`'${password}'`);
        }else{
            values.push(`'${obj[key]}'`);
        }
    });

    query.push(`( ${values.join(', ')} )`);

    return query.join(' ');
}

export const SQLLOGIN = (email:string ) => {
    const query = `SELECT * FROM users WHERE email='${email}' AND status=1`;
    return query;
};