import pool from '../../database/config';
import { s3, DATAS3 } from '../../helpers/aws-config';
import { Users } from './users.model';
import {
    SQLUPDATEUSERS,
    SQLINSERTUSERS,
    SQLUSERS,
    SQLUSER,
    SQLUSERBYEMAIL,
    SQLLOGIN
} from './users.constant';
import ConstantQuery from '../../global/constant-query';

const constantQuery = new ConstantQuery("users");

export default class UsersServices {

    private client;

    constructor() {
        this.client = pool;
    }

    public async createUsers(obj:any){
        try {
            if (Object.entries(obj).length === 0) throw new Error("El objeto users esta vacio");
            
            let query = SQLUSERBYEMAIL(obj.email);
            const { rows } = await this.client.query(query);
            
            if (rows.length !== 0){
                return "El usuario ya existe";
            }

            let insert = SQLINSERTUSERS(obj);
            await this.client.query(insert);
            return "Creado correctamente";

        } catch (error) {
            throw new Error("Error al crear usuarios: "+error);
        }
    }

    public async updateUsers(id:number, obj:any){
        try {

            if (isNaN(id)){
                return "El usuario no existe";
            }

            let update = SQLUPDATEUSERS(id,obj);
            await this.client.query(update);
            return "Actulizado correctamente";
        } catch (error) {
            throw new Error("Error al actulizar usuarios: "+error);
        }
    }

    public async getUsers(limit:number = 8, page:number, filters:Object){
        try {
            let countRows = 0;

            let queryRows = constantQuery.SQLCOUNTROWS("id","status", 1, filters);
            const result = await this.client.query(queryRows);

            const params:Object = {},
                innerJoins:Object ={},
                whereOpc:Object = { status: 1},
                orderBy:string = 'users.id ASC';
        
            const query = constantQuery.SQLGET(
                limit,
                page,
                filters,
                params,
                innerJoins,
                whereOpc,
                orderBy
            );

            const rows = await this.client.query(query);

            
            countRows = Math.ceil(result.rows[0].count/limit);
            

            const data = {
                limit,
                page,
                numpages:countRows,
                clientes:rows.rows
            };

            return data;
        } catch (error) {
            throw new Error("Error al consultar usuarios: "+error);
        }
    }

    public async getUser(id: number): Promise<Users> {
        try {
            const query = SQLUSER(id);
            const {rows} = await this.client.query(query);
            return rows[0];
        } catch (error) {
            throw new Error("Error al consultar usuarios: "+error);
        }
    }

    public async login( email: string): Promise<Users> {
        try {
            const query = SQLLOGIN(email);
            const { rows } = await this.client.query(query);
            return rows[0];
        } catch (error) {
            throw new Error("Error al consultar usuarios: "+error);
        }
    }

    public async uploadImage(obj:any){
        try {
            let id = obj.id;
            let name = obj.name;
            let filename = obj.filename;
            
            const datas3 = DATAS3(`usuarios/${name}`, filename);

            let location = '';
            const { Location } = await s3.upload(datas3).promise();
            location = Location;

            const data = {
                imagen: location
            };

            const query = SQLUPDATEUSERS(id, data);
            await this.client.query(query);

            return "Imagen agregada al modelo con exito";
        } catch (error) {
            console.log(error);
            throw new Error("Error no se pudo guardar imagen");
        }
    }

}
