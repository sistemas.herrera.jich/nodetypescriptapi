import { Router } from 'express';
import AuthController from './auth.controller';
import { verification_JWT } from '../../middlewares/authenticated-jwt';
const router = Router();

export class AuthRoutes {
    private authController: AuthController;
    
    constructor() {
        this.authController = new AuthController();
    }

    public get routes(){
        let controller = this.authController;
        
        router.post('/login',controller.login);
        router.get('/verification',[ verification_JWT ],controller.verify);

        return router;
    }
}