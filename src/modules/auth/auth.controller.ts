import bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import httpStatus from 'http-status';
import UsersServices from '../users/users.service';
import { generate_jwt } from './jwt';

let services = new UsersServices();

class AuthController {
    
    async login(req: Request, res: Response){
        try {
            const {email, password } = req.body;
            const user = await services.login(email);
            
            if (user.id) {
                const pwd = bcrypt.compareSync(password, user.password);
                if ( !pwd ) {
                    return res.status(httpStatus.BAD_REQUEST).json({
                        isValid:false,
                        message: `Credenciales incorrectas`
                    });
                }

                const token = await generate_jwt(user);

                res.status(httpStatus.OK).json({
                    isValid:true,
                    message: `Login correcto`,
                    data: user,
                    token: token
                });
                
            } else {
                res.status(httpStatus.BAD_REQUEST).json({
                    isValid:false,
                    message: `Credenciales incorrectas`
                });
            }
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message:` Credenciales incorrectas`
            });
        }
    }

    async verify(req: Request, res: Response){
        try {
            const { sub } = req.body;
            const user = await services.getUser(sub);
            const token = await generate_jwt(user);

            res.status(httpStatus.OK).json({
                isValid:true,
                message: `Verificacion de token correcta`,
                data: user,
                token: token
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message:error
            });
        }
    }
}

export default AuthController;
