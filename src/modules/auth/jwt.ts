import jwt from 'jsonwebtoken';
import moment from 'moment';
import { Users } from '../users/users.model';

export const generate_jwt = (user: Users) => {
    return new Promise((resolve, reject) => {
        let payload = {
            sub: user.id,
            nombre: user.nombre,
            apellidos: user.apellidos,
            email: user.email,
            tipo: user.tipo,
            iat:moment().unix(),
            exp:moment().add(12,'hours').unix()
        }
        let secret = process.env.SECRET_KEY || '';
        jwt.sign(payload,secret,(err, token) => {
            if(err) {
                reject(`Can't create token ${err}`);
            }else{
                resolve(token);
            }
        });
    });
}