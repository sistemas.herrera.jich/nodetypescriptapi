import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ClientesServices from './clientes.service';

const service = new ClientesServices();

export default class ClientesController{

    public async getClientes(req: Request, res: Response){
        try {
            const body = req.body;
            const limit = parseInt(req.params.limit);
            const page = parseInt(req.params.page);

            const clientes = await service.getClientes(limit,page,body);

            res.status(httpStatus.OK).json({
                isValid: true,
                message: "Listado Clientes",
                data: clientes
            });

        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error en la consulta"
            });
        }
    }

    public async getSaldos(req: Request, res: Response){
        try {
            const id = parseInt(req.params.id);
            const saldos = await service.getSaldoClientes(id);

            res.status(httpStatus.OK).json({
                isValid: true,
                message:"Saldos clientes",
                data: saldos
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error en la consulta" + error
            });
        }
    }

    public async getSaldosAdmin(req: Request, res: Response){
        try {
            const id = parseInt(req.params.id);
            const saldos = await service.getSaldoClientes(id);

            res.status(httpStatus.OK).json({
                isValid: true,
                message:"Saldos clientes admin",
                data: saldos
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error al obtener saldos" + error
            });
        }
    }

    public async getFacturas(req: Request, res: Response){
        try {
            const id = parseInt(req.params.id);
            const facturas = await service.getFacturasCliente(id);

            res.status(httpStatus.OK).json({
                isValid:true,
                message: "Listado de facturas",
                data:facturas
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error en la consulta" + error
            });
        }
    }

    public async addPDFCif(req: Request, res: Response){
        try {
            const id = req.body.id;
            const pdf = req.body.pdf;

            const dataimg = pdf.split(';');
            const mimetype = dataimg[0].split(':');
            const type = mimetype[1].split('/');
            const base64 = dataimg[1].split(',');
            const buff = Buffer.from(base64[1], 'base64');

            const obj = {
                id:id,
                name:`cif_pdf_${id}.${type[1]}`,
                buff:buff,
                mimetype: mimetype
            }

            const responce = await service.addPDFCif(obj);

            res.status(httpStatus.OK).json({
                isValid: true,
                message: responce
            });

        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: error
            });
        }
    }

    public async createClientes(req: Request, res: Response){
        try {
            const { razon_social, rfc, pdf } = req.body;
            
            const obj = {
                razon_social,
                rfc
            };

            if (Object.entries(obj).length === 0) {
                return res.status(httpStatus.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se puede agregar objeto vacio"
                });
            }

            const responce = await service.createClientes(obj);

            const data = await service.getClientes(1,1,{"rfc":rfc });

            const dataimg = pdf.split(';');
            const mimetype = dataimg[0].split(':');
            const type = mimetype[1].split('/');
            const base64 = dataimg[1].split(',');
            const buff = Buffer.from(base64[1], 'base64');

            const id = data.clientes.id;

            const file = {
                id:id,
                name:`cif_pdf_${id}.${type[1]}`,
                buff:buff,
                mimetype: mimetype
            }

            const message = await service.addPDFCif(file);

            res.status(httpStatus.OK).json({
                isValid: true,
                message: `Alta: ${responce}, PDF: ${message}`,
                data:data
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message:"Error al agregar cliente"
            });
        }
    }

}