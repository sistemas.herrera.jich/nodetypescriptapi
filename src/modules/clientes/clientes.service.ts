import pool from '../../database/config';
import ConstantQuery from '../../global/constant-query';
import { s3, DATAS3 } from '../../helpers/aws-config';

const constantQuery = new ConstantQuery("clientes");

export default class ClienteService{
    private client;

    constructor(){
        this.client = pool;
    }

    public async getClientes(limit:number = 8, page:number, filters:Object){
        try {
            let countRows = 0;

            let queryRows = constantQuery.SQLCOUNTROWS("id","status", 1, filters);
            console.log(queryRows);
            const result = await this.client.query(queryRows);

            const params:Object = {1:"fecha", 2:"folio", 3:"rfc", 4:"razon_social", 5:"id"},
                innerJoins:Object ={},
                whereOpc:Object = { status: 1},
                orderBy:string = 'clientes.fecha ASC';
        
            const query = constantQuery.SQLGET(
                limit,
                page,
                filters,
                params,
                innerJoins,
                whereOpc,
                orderBy
            );

            const rows = await this.client.query(query);

            countRows = Math.ceil(result.rows[0].count/limit);

            const data = {
                limit,
                page,
                numpages:countRows,
                clientes:rows.rows
            };

            return data;

        } catch (error) {
            throw new Error("Error en la consulta"+ error);
        }
    }

    public async addPDFCif(file){
        try {
            let id = file.id;
            let name = file.name;
            let buff = file.buff;
            let mimetype = file.mimetype;

            const datas3 = DATAS3(`clientes/cif/${name}`,buff, mimetype);
            
            const { Location } = await s3.upload(datas3).promise();

            const data = { 
                pdf: Location
            }

            const query = constantQuery.SQLUPDATE(data, id);
            await this.client.query(query);

            return "PDF Guardado correctamente";

        } catch (error) {
            throw new Error("Error en la consulta " + error);
        }
    }

    public async createClientes(obj: any){
        try {
            if (Object.entries(obj).length === 0) throw new Error("El objeto users esta vacio");

            const rfc = obj.rfc;
            const query = constantQuery.SQLGETALL("rfc", rfc);
            const { rows } = this.client.query(query);

            if (rows.length === 0){
                return "Ya existe el cliente";
            }

            const insert = constantQuery.SQLINSERT(obj);
            await this.client.query(insert);

            return "Clientes correctamente creado";

        } catch (error) {
            throw new Error("Error en consulta "+ error);
        }
    }

    public async getSaldoClientes(idAgente:number){
        try {
            const query = `select clientes.idadminpaq as id, clientes.razon_social as cliente, clientes.rfc as rfc, users.nombre as agente,
            sum(documentos.pendiente) as saldo
            from clientes
            left join documentos on documentos.idcliente = clientes.idadminpaq
            inner join users on users.idadminpaq = clientes.agente_venta 
            where documentos.pendiente::numeric::int > 3 and users.id = ${idAgente}
            group by clientes.idadminpaq, clientes.razon_social, clientes.rfc, users.nombre
            order by saldo desc `;

            const saldos = await this.client.query(query);
            
            return saldos.rows;
        } catch (error) {
            throw new Error("Error en la consulta" + error)
        }
    }

    public async getSaldosAdmin(){
        try {
            const query = `select clientes.idadminpaq as id, clientes.razon_social as cliente, clientes.rfc as rfc,
            sum(documentos.pendiente) as pendiente
            from clientes
            left join documentos on documentos.idcliente = clientes.idadminpaq 
            where documentos.pendiente::numeric::int > 3
            group by clientes.idadminpaq, clientes.razon_social, clientes.rfc
            order by pendiente DESC `;

            const saldos = await this.client.query(query);
            
            return saldos.rows;
        } catch (error) {
            throw new Error("Error en la consulta" + error)
        }
    }

    public async getFacturasCliente( idCliente: number){
        try {
            const query = `select clientes.idadminpaq as id, 
            clientes.razon_social as cliente, 
            clientes.rfc as rfc,
            documentos.pendiente as saldo,
            documentos.folio as folio,
            documentos.observaciones as observaciones
            from clientes
            inner join documentos on documentos.idcliente = clientes.idadminpaq
            where documentos.pendiente::numeric::int > 3 and clientes.idadminpaq = ${idCliente}
            group by documentos.folio, documentos.pendiente,clientes.idadminpaq, clientes.razon_social, clientes.rfc, documentos.observaciones
            order by saldo desc`;

            const facturas = await this.client.query(query);

            return facturas.rows;
        } catch (error) {
            throw new Error("Error en "+ error);
        }
    }

}
