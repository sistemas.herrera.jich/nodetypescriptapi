import { Router } from 'express';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
import ClientesController from './clientes.controller';

const router = Router();

export class ClientesRoutes{
    private clientesController: ClientesController;

    constructor(){
        this.clientesController = new ClientesController();
    }

    public get routes(){
        let controller = this.clientesController;

        router.post('/all/:limit/:page', controller.getClientes);
        router.post('/create', controller.createClientes);
        router.put('/addPdf',controller.addPDFCif);
        router.get('/saldos/:id',[ authenticated_JWT ], controller.getSaldos);
        router.get('/facturas/:id',[ authenticated_JWT ], controller.getFacturas);
        router.get('/saldos_admin',[ authenticated_JWT ], controller.getSaldosAdmin);
        return router;
    }
}