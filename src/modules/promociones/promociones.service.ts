import pool from '../../database/config';
import ConstantQuery from './promociones.constant';

const constantQuery = new ConstantQuery("promociones");

export default class PromocionesService {
    private client;

    constructor(){
        this.client = pool;
    }

    public async getPromociones(limit:number = 8, page:number, filters:Object){

        try {
            let countRows = 0;

            let queryRows = constantQuery.SQLCOUNTROWS("id","status", 1, filters);

            const result = await this.client.query(queryRows);

            const params:Object = {1:"url", 2:"descripcion", 3:"create_at", 4:"expiration_date", 5:"id", 6:"codigo"},
                innerJoins:Object ={},
                whereOpc:Object = { status: ` = 1`, expiration_date:` > '${new Date(Date.now()).toISOString()}'` },
                orderBy:string = 'promociones.expiration_date DESC';

            const query = constantQuery.SQLGET(
                limit,
                page,
                filters,
                params,
                innerJoins,
                whereOpc,
                orderBy
            );

            const rows = await this.client.query(query);

            countRows = Math.ceil(result.rows[0].count/limit);

            const data = {
                limit,
                page,
                numpages:countRows,
                promociones:rows.rows
            };

            return data;

        } catch (error) {
            throw new Error("Error en la consulta"+ error);
        }
    }
}