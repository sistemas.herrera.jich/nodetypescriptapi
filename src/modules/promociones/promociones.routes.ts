import { Router } from 'express';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
import PromocionesController from './promociones.controller';

const router = Router();

router.use(authenticated_JWT);

export class PromocionesRoutes{
    private promociones:PromocionesController;

    constructor(){
        this.promociones = new PromocionesController();
    }

    public get routes(){
        let controller = this.promociones;

        router.post('/all/:limit/:page', controller.getPromociones);

        return router;
    }
}