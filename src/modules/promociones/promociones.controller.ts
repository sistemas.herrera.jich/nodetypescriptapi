import { Request, Response } from 'express';
import httpStatus from 'http-status';
import PromocionesServices from './promociones.service';

const service = new PromocionesServices();

export default class PromocionesController{

    public async getPromociones(req: Request, res: Response){
        try {
            const body = req.body;
            const limit = parseInt(req.params.limit);
            const page = parseInt(req.params.page);

            const clientes = await service.getPromociones(limit,page,body);

            res.status(httpStatus.OK).json({
                isValid: true,
                message: "Listado Promociones",
                data: clientes
            });

        } catch (error) {
            console.log(error);
            res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: "Error en la consulta"
            });
        }
    }
    
}