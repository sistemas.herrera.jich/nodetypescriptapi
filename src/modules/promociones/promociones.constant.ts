export default class ConstantQuerys{
    private table:string;

    constructor(table:string){
        this.table = table;
    }

    public SQLINSERT(obj:any){
        const table = this.table;

        let query:string[] = [`INSERT INTO ${table}`];
        let cols:string[] = [];
        Object.keys(obj).forEach(function(key, i){
            cols.push(key);
        });

        query.push(`( ${cols.join(', ')} ) VALUES`);

        let values:string[] = [];
        Object.keys(obj).forEach(function (key,i){
            values.push(`'${obj[key]}'`);
        });

        query.push(`( ${values.join(', ')} )`);

        return query.join(' ');
    }

    public SQLUPDATE(obj:any, id:any) {
        const table = this.table;

        let query:string[] = [`UPDATE ${table} SET`];
        let set:string[] = [];
        Object.keys(obj).forEach(function (key,i){
            let item = `${key}='${obj[key]}'`;
            set.push(item);
        });
        query.push(set.join(', '));
    
        query.push(`WHERE id=${id}`);
    
        return query.join(' ');
    }
    
    public SQLGETBYONE(item:string, value:any) {
        let val = isNaN(value) === false ? `${value}`: `'${value}'`
        const query = `SELECT * FROM ${this.table} WHERE ${item} = ${val}`;
        return query;
    }

    public SQLGETALL(status:string, val:any){
        const query = `SELECT * FROM ${this.table} WHERE ${status} = ${val}`;
        return query;
    }

    //return query with pagination, params optional, filters optional, where optional etc
    public SQLGET(
        limit:number,
        page:number,
        filters:Object,
        params:Object = {},
        innerJoins:Object ={},
        whereOpc:Object = {},
        orderBy:string = '', /** Example table.fields DESC | table.fields ASC */
    ){
        const tb = this.table;

        let query:string[] = ['SELECT']; //Init
        let values:string[] = []; //Params for select fields
        let inners:string[] = []; //Inner joins if exist
        let wheres:string[] = []; // If where conditions exist

        //Params fields
        if(Object.entries(params).length !== 0){

            Object.keys(params).forEach(function (key,i){
                values.push(`${params[key]}`);
            });

            query.push(` ${values.join(', ')} FROM ${tb} `);
        } else {
            query.push(` * FROM ${tb} `);
        }

        //Inner join's
        if(Object.entries(innerJoins).length !== 0){
            Object.keys(innerJoins).forEach(function(key, i){
                inners.push(`INNER JOIN ${key} ON ${innerJoins[key]}`);
            });
            query.push(` ${inners.join('\n ')} `);
        }

        //Where's
        if(Object.entries(whereOpc).length !== 0){
            Object.keys(whereOpc).forEach(function (key,i){
                if(i === 0){
                    wheres.push(` ${tb}.${key} ${whereOpc[key]}`);
                }else{
                    wheres.push(`AND ${tb}.${key} ${whereOpc[key]}`);
                }
            });
            query.push(` WHERE ${wheres.join(' ')} `);
        }

        //Like if exist filters
        if(Object.entries(filters).length){
            let set:string[] = [];

            Object.keys(filters).forEach(function (key,i){
                if(isNaN(filters[key])){
                    let item = `AND ${key} LIKE '%${filters[key]}%'`;
                    set.push(item);
                } else {
                    let item = `AND CAST(${key} AS TEXT) LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
                
            });

            query.push(set.join(' '));
        }

        //Order by
        if(orderBy !== ''){
            query.push(` ORDER BY ${orderBy} `);
        }

        query.push(` LIMIT ${limit} OFFSET ((${page} - 1 ) * ${limit});`);

        return query.join(' ');
    }

    public SQLCOUNTROWS(item:string, status:string, val:any, filters:Object={}, operator:string="="){
        const tb = this.table;
        let query:string[] = [`SELECT count(${item}) FROM ${tb} WHERE ${status} ${operator} ${val}`];
        //Like if exist filters
        if(Object.entries(filters).length){
            let set:string[] = [];

            Object.keys(filters).forEach(function (key,i){
                if(isNaN(filters[key])){
                    let item = `AND ${key} LIKE '%${filters[key]}%'`;
                    set.push(item);
                } else {
                    let item = `AND CAST(${key} AS TEXT) LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
                
            });

            query.push(set.join(' '));
        }

        return query.join(' ');
    }

}