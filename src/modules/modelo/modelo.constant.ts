export const SQLINSERT = (obj:any) => {
    let query:string[] = ['INSERT INTO modelo'];
    let cols:string[] = [];
    Object.keys(obj).forEach(function(key, i){
        cols.push(key);
    });

    query.push(`( ${cols.join(', ')} ) VALUES`);

    let values:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        values.push(`'${obj[key]}'`);
    });

    query.push(`( ${values.join(', ')} )`);

    return query.join(' ');
}

export const SQLUPDATE = (id:number, obj:any) => {
    let query:string[] = ['UPDATE modelo SET'];
    let set:string[] = [];
    Object.keys(obj).forEach(function (key,i){
        let item = `${key}='${obj[key]}'`;
        set.push(item);
    });
    query.push(set.join(', '));

    query.push(`WHERE id=${id}`);

    return query.join(' ');
}

export const SQLMODELOS = () => {
    const query = `SELECT * FROM modelo`;
    return query;
}

export const SQLMODELO = (id:string) => {
    const query = `SELECT * FROM modelo WHERE id=${id}`;
    return query;
}

