import pool from "../../database/config";
import ConstantQuery from '../../global/constant-query';
import { s3, DATAS3 } from '../../helpers/aws-config';
import { limpiar } from '../../helpers/base64-destructure';

import {
    SQLINSERT,
    SQLUPDATE,
    SQLMODELOS,
    SQLMODELO
} from './modelo.constant';

const constantQuery = new ConstantQuery("modelo");

export default class ModeloServices {
    private client;
    constructor() {
        this.client = pool;
    }

    public async createModel(model:Object){
        try {
            if (Object.entries(model).length === 0 ) throw new Error("Modelo no puede ir vacio");

            const insert = SQLINSERT(model);
            await this.client.query(insert);
            return "Modelo creado correctamente";
        } catch (error) {
            throw new Error("Error al crear modelo")
        }
    }

    public async updateModel(id:number, model:Object){
        try {
            if (Object.entries(model).length === 0) throw new Error("Modelo no puede ir vacio");
            
            const update = SQLUPDATE(id, model);
            await this.client.query(update);
            return "Modelo actulizado correctamente";
        } catch (error) {
            throw new Error("Error al actulizar modelo")
        }
    }

    public async getModelsTable(limit: number =8, page: number, filters: Object){
        try {
            let countRows = 0;

            let queryRows = constantQuery.SQLCOUNTROWS("id","status", 1, filters);
            const result = await this.client.query(queryRows);

            const params:Object = {1:"nombre", 2:"url", 3:"id"},
                innerJoins:Object ={},
                whereOpc:Object = { status: 1},
                orderBy:string = 'modelo.nombre ASC';

            const query = constantQuery.SQLGET(
                limit,
                page,
                filters,
                params,
                innerJoins,
                whereOpc,
                orderBy
            );

            const rows = await this.client.query(query);

            countRows = Math.ceil(result.rows[0].count/limit);

            const data = {
                limit,
                page,
                numpages:countRows,
                data:rows.rows
            };

            return data;
        } catch (error) {
            throw new Error("Error al traer los modelos");
        }
    }

    public async getModels(){
        try {
            const query = SQLMODELOS();
            const { rows } = await this.client.query(query);
            return rows;
        } catch (error) {
            throw new Error("Error al traer los modelos");
        }
    }


    public async getModel(id:string){
        try {
            const query = SQLMODELO(id);
            const { rows } = await this.client.query(query);
            return rows[0];
         } catch (error) {
            throw new Error("Error al traer los modelos");
        }
    }

    public async uploadImage(obj:any) {
        try {
            let id = obj.id;
            let name = obj.name;
            let filename = obj.filename;

            const b64clean = limpiar(filename);
            const datas3 = DATAS3(`productos/${name}.${b64clean.type}`, b64clean.buff, b64clean.mimetype);
            let location = '';
            const { Location } = await s3.upload(datas3).promise();
            location = Location;

            const data = {
                url: location
            };

            const query = SQLUPDATE(id, data);
            await this.client.query(query);

            return "Imagen agregada al modelo con exito";
        } catch (error) {
            throw new Error("Imagen no se guardo correctamente"+ error);
        }
    }
}