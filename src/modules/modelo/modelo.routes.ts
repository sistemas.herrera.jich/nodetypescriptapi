import { Router } from 'express';
import { authenticated_JWT } from '../../middlewares/authenticated-jwt';
import ModeloController from './modelo.controller';
const router = Router();

router.use(authenticated_JWT);

export class ModeloRoutes {
    private modeloController: ModeloController
    constructor(){
        this.modeloController = new ModeloController();
    }

    public get routes(){
        let controller = this.modeloController;

        router.post('/create', controller.createModel);
        router.post('/getModelTable/:limit/:page', controller.getModelsTable)
        router.put('/update/:id', controller.updateModel);
        router.get('/getAll', controller.getModels);
        router.get('/getOne/:id', controller.getModel);
        router.post('/addImage',controller.addImage);
        
        return router
    }
}