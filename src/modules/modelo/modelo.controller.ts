import { Request, Response } from 'express';
import httpStatus from 'http-status';
import ModeloServices from './modelo.service';

const services = new ModeloServices();

export default class ModeloController {

    private validateObj(obj:Object){
        if (Object.entries(obj).length === 0) {
            return true;
        }else{
            return false;
        }
    }

    public async createModel(req: Request, res: Response){
        try {
            const { modelo } = req.body;
            
            if (this.validateObj(modelo)) {
                res.status(httpStatus.BAD_REQUEST).json({
                    isValid: false,
                    message: "El modelo no puede ir vacio"
                });
            }

            const msg = await services.createModel(modelo);
            const dataModel = await services.getModels();

            res.status(httpStatus.OK).json({
                isValid: true,
                message: msg,
                data: dataModel
            });

        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al crear modelo ${error}`
            })
        }
    }

    public async updateModel(req: Request, res: Response){
        try {
            const id = parseInt(req.params.id);
            const { modelo } = req.body;

            if (this.validateObj(modelo)) {
                res.status(httpStatus.BAD_REQUEST).json({
                    isValid: false,
                    message: "El modelo no puede ir vacio"
                });
            }

            const msg = await services.updateModel(id, modelo);
            const modelos = await services.getModels();

            res.status(httpStatus.OK).json({
                isValid: true,
                message: msg,
                data: modelos
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al actualizar modelo ${error}`
            });
        }
    }

    public async getModels( req: Request, res: Response){
        try {
            const body = req.body;
            const limit = parseInt(req.params.limit);
            const page = parseInt(req.params.page);

            const modelo = await services.getModels();

            res.status(httpStatus.OK).json({
                isValid: true,
                message:'Listado de modelos',
                data: modelo
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al consultar modelo ${error}`
            });
        }
    }

    public async getModelsTable( req: Request, res: Response){
        try {
            const body = req.body;
            const limit = parseInt(req.params.limit);
            const page = parseInt(req.params.page);

            const modelo = await services.getModelsTable(limit, page, body);

            res.status(httpStatus.OK).json({
                isValid: true,
                message:'Listado de modelos',
                data: modelo
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al consultar modelo ${error}`
            });
        }
    }

    public async getModel( req: Request, res: Response) {
        try {
            const id = req.params.id;
            const model = await services.getModel(id);
            res.status(httpStatus.OK).json({
                isValid:true,
                message: 'Modelo encontrado',
                data: model
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al consultar modelo ${error}`
            })
        }
    }

    public async addImage(req: Request, res: Response){
        try {
            const { id, filename } = req.body;
            const data ={
                id,
                name:`image_${id}`,
                filename
            }
            const msg = await services.uploadImage(data);
            const modelos = await services.getModels();

            res.status(httpStatus.OK).json({
                isValid: true,
                message: msg,
                data: modelos
            });
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json({
                isValid: false,
                message: `Error al subir imagen a modelo ${error}`
            })
        }
    }

}