export * from '../modules/users/users.routes';
export * from '../modules/auth/auth.routes';
export * from '../modules/productos/productos.routes';
export * from '../modules/modelo/modelo.routes';
export * from '../modules/clientes/clientes.routes';
export * from '../modules/promociones/promociones.routes';
export * from '../modules/apartado/apartado.routes';