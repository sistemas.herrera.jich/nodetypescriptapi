import aws from 'aws-sdk';

const aws_config = {
    "accessKeyId":process.env.S3IDCLAVEACCESO,
    "secretAccessKey":process.env.S3CLAVEACCESO,
    "region":process.env.S3REGION,
}

export const DATAS3 = (key:string, body:any, mimetype:string='text/plain') => {
    return {
        Bucket:process.env.S3BUCKEDNAME || '',
        Key:key,
        Body:body,
        ContentType: mimetype
    }
}

export const s3 = new aws.S3(aws_config);