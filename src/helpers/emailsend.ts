import handlebars from "handlebars";
import path from "path";
import * as fs from 'fs';

export const EMAIL = (obj: any) => {
    const filePath = path.join(__dirname, '../template/emailapartado.html');
    const source = fs.readFileSync(filePath, 'utf8').toString();
    const template = handlebars.compile(source);
    const replacements = {
        vendedor: obj.vendedor,
        razonSocial: obj.razonSocial,
        domicilio: obj.domicilio,
        telefono: obj.telefono,
        notes: obj.notes,
        url: obj.url,
        codigo: obj.codigo,
        cantidad: obj.cantidad,
        productoName: obj.productoName,
        precio: obj.precio
    };
    const htmlToSend = template(replacements);
    return htmlToSend;
};