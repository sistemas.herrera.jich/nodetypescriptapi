export const limpiar = (base64Img: string) => {
    try {
      const dataimg = base64Img.split(';');
      const mimetype = dataimg[0].split(':');
      const type = mimetype[1].split('/');
      const base64 = dataimg[1].split(',');
      const buff = Buffer.from(base64[1], 'base64');

      return {
        mimetype: mimetype[1],
        type: type[1],
        buff: buff,
      };
    } catch (error) {
      //console.log(error);
      throw new Error("No se pudo prosesar: " + base64Img);
    }
  }