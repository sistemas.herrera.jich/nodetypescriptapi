import { Pool } from 'pg';

const pool = new Pool({
    host: process.env.HOST,
    user: process.env.USERDB,
    password: process.env.PASSWORD,
    database : process.env.DATABASE,
    port: parseInt(process.env.DBPORT || ''),
    max: parseInt(process.env.MAX || ''),
});

export default pool;