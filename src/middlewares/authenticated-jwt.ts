import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';

export const authenticated_JWT = (req: Request, res: Response, next: NextFunction) => {
    try {
        let token = req.headers['authorization'];

        if (typeof token === 'undefined') {
            return res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: 'Invalid token undefined'
            });
        }

        let tokenComparation = token.split(' ');
        token = tokenComparation[1];
        let secret = process.env.SECRET_KEY || '';
        const { sub } = jwt.verify(token, secret);
        next();

    } catch (error) {
        return res.status(httpStatus.BAD_REQUEST).json({
            isValid:false,
            message: 'Invalid token unauthorized ' +error
        });
    }
}

export const verification_JWT = (req: Request, res: Response, next: NextFunction) => {
    try {
        let token = req.headers['authorization'];

        if (typeof token === 'undefined') {
            return res.status(httpStatus.BAD_REQUEST).json({
                isValid:false,
                message: 'Invalid token undefined'
            });
        }

        let tokenComparation = token.split(' ');
        token = tokenComparation[1];
        let secret = process.env.SECRET_KEY || '';
        const { sub } = jwt.verify(token, secret);
        req.body = {sub};
        next();

    } catch (error) {
        return res.status(httpStatus.BAD_REQUEST).json({
            isValid:false,
            message: 'Invalid token unauthorized ' +error
        });
    }
}
