"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SQLMODELO = exports.SQLMODELOS = exports.SQLUPDATE = exports.SQLINSERT = void 0;
const SQLINSERT = (obj) => {
    let query = ['INSERT INTO modelo'];
    let cols = [];
    Object.keys(obj).forEach(function (key, i) {
        cols.push(key);
    });
    query.push(`( ${cols.join(', ')} ) VALUES`);
    let values = [];
    Object.keys(obj).forEach(function (key, i) {
        values.push(`'${obj[key]}'`);
    });
    query.push(`( ${values.join(', ')} )`);
    return query.join(' ');
};
exports.SQLINSERT = SQLINSERT;
const SQLUPDATE = (id, obj) => {
    let query = ['UPDATE modelo SET'];
    let set = [];
    Object.keys(obj).forEach(function (key, i) {
        let item = `${key}='${obj[key]}'`;
        set.push(item);
    });
    query.push(set.join(', '));
    query.push(`WHERE id=${id}`);
    return query.join(' ');
};
exports.SQLUPDATE = SQLUPDATE;
const SQLMODELOS = () => {
    const query = `SELECT * FROM modelo`;
    return query;
};
exports.SQLMODELOS = SQLMODELOS;
const SQLMODELO = (id) => {
    const query = `SELECT * FROM modelo WHERE id=${id}`;
    return query;
};
exports.SQLMODELO = SQLMODELO;
//# sourceMappingURL=modelo.constant.js.map