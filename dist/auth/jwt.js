"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generate_jwt = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const moment_1 = __importDefault(require("moment"));
const generate_jwt = (user) => {
    return new Promise((resolve, reject) => {
        let payload = {
            sub: user.id,
            nombre: user.nombre,
            apellidos: user.apellidos,
            email: user.email,
            tipo: user.tipo,
            iat: (0, moment_1.default)().unix(),
            exp: (0, moment_1.default)().add(12, 'hours').unix()
        };
        let secret = process.env.SECRET_KEY || '';
        jsonwebtoken_1.default.sign(payload, secret, (err, token) => {
            if (err) {
                reject(`Can't create token ${err}`);
            }
            else {
                resolve(token);
            }
        });
    });
};
exports.generate_jwt = generate_jwt;
//# sourceMappingURL=jwt.js.map