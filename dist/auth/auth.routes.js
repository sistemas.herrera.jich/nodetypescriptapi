"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthRoutes = void 0;
const express_1 = require("express");
const auth_controller_1 = __importDefault(require("./auth.controller"));
const authenticated_jwt_1 = require("../middlewares/authenticated-jwt");
const router = (0, express_1.Router)();
class AuthRoutes {
    constructor() {
        this.authController = new auth_controller_1.default();
    }
    get routes() {
        let controller = this.authController;
        router.post('/login', controller.login);
        router.get('/verification', [authenticated_jwt_1.verification_JWT], controller.verify);
        return router;
    }
}
exports.AuthRoutes = AuthRoutes;
//# sourceMappingURL=auth.routes.js.map