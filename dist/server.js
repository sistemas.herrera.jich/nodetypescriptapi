"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const path_1 = __importDefault(require("path"));
const routes_1 = require("./routes");
class Server {
    constructor() {
        this.start = (port) => {
            return new Promise((resolve, reject) => {
                this.app.listen(port, () => {
                    resolve(port);
                }).on('error', (err) => {
                    reject(err);
                });
            });
        };
        this.app = (0, express_1.default)();
        this.config();
        this.routeConfig();
    }
    config() {
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.use(express_1.default.urlencoded({ extended: true, limit: '50mb' }));
        const publicPath = path_1.default.join(__dirname, 'public');
        this.app.use(express_1.default.static(publicPath));
    }
    routeConfig() {
        let usersRoutes = new routes_1.UsersRoutes();
        let authRoutes = new routes_1.AuthRoutes();
        let productosRoutes = new routes_1.ProductosRoutes();
        let modeloRoutes = new routes_1.ModeloRoutes();
        let clientesRoutes = new routes_1.ClientesRoutes();
        let promocionesRoutes = new routes_1.PromocionesRoutes();
        let apartado = new routes_1.ApartadoRoutes();
        this.app.use('/users', usersRoutes.routes);
        this.app.use('/auth', authRoutes.routes);
        this.app.use('/productos', productosRoutes.routes);
        this.app.use('/modelo', modeloRoutes.routes);
        this.app.use('/clientes', clientesRoutes.routes);
        this.app.use('/promociones', promocionesRoutes.routes);
        this.app.use('/apartado', apartado.routes);
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map