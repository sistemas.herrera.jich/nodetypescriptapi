"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
const pool = new pg_1.Pool({
    host: process.env.HOST,
    user: process.env.USERDB,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    port: parseInt(process.env.DBPORT || ''),
    max: parseInt(process.env.MAX || ''),
});
exports.default = pool;
//# sourceMappingURL=config.js.map