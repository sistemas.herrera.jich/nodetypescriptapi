"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EMAIL = void 0;
const handlebars_1 = __importDefault(require("handlebars"));
const path_1 = __importDefault(require("path"));
const fs = __importStar(require("fs"));
const EMAIL = (obj) => {
    const filePath = path_1.default.join(__dirname, '../template/emailapartado.html');
    const source = fs.readFileSync(filePath, 'utf8').toString();
    const template = handlebars_1.default.compile(source);
    const replacements = {
        vendedor: obj.vendedor,
        razonSocial: obj.razonSocial,
        domicilio: obj.domicilio,
        telefono: obj.telefono,
        notes: obj.notes,
        url: obj.url,
        codigo: obj.codigo,
        cantidad: obj.cantidad,
        productoName: obj.productoName,
        precio: obj.precio
    };
    const htmlToSend = template(replacements);
    return htmlToSend;
};
exports.EMAIL = EMAIL;
//# sourceMappingURL=emailsend.js.map