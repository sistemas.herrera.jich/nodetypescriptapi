"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ConstantQuerys {
    constructor(table) {
        this.table = table;
    }
    SQLINSERT(obj) {
        const table = this.table;
        let query = [`INSERT INTO ${table}`];
        let cols = [];
        Object.keys(obj).forEach(function (key, i) {
            cols.push(key);
        });
        query.push(`( ${cols.join(', ')} ) VALUES`);
        let values = [];
        Object.keys(obj).forEach(function (key, i) {
            values.push(`'${obj[key]}'`);
        });
        query.push(`( ${values.join(', ')} )`);
        return query.join(' ');
    }
    SQLUPDATE(obj, id) {
        const table = this.table;
        let query = [`UPDATE ${table} SET`];
        let set = [];
        Object.keys(obj).forEach(function (key, i) {
            let item = `${key}='${obj[key]}'`;
            set.push(item);
        });
        query.push(set.join(', '));
        query.push(`WHERE id=${id}`);
        return query.join(' ');
    }
    SQLGETBYONE(item, value) {
        let val = isNaN(value) === false ? `${value}` : `'${value}'`;
        const query = `SELECT * FROM ${this.table} WHERE ${item} = ${val}`;
        return query;
    }
    SQLGETALL(status, val) {
        const query = `SELECT * FROM ${this.table} WHERE ${status} = ${val}`;
        return query;
    }
    SQLGET(limit, page, filters, params = {}, innerJoins = {}, whereOpc = {}, orderBy = '') {
        const tb = this.table;
        let query = ['SELECT'];
        let values = [];
        let inners = [];
        let wheres = [];
        if (Object.entries(params).length !== 0) {
            Object.keys(params).forEach(function (key, i) {
                values.push(`${params[key]}`);
            });
            query.push(` ${values.join(', ')} FROM ${tb} `);
        }
        else {
            query.push(` * FROM ${tb} `);
        }
        if (Object.entries(innerJoins).length !== 0) {
            Object.keys(innerJoins).forEach(function (key, i) {
                inners.push(`INNER JOIN ${key} ON ${innerJoins[key]}`);
            });
            query.push(` ${inners.join('\n ')} `);
        }
        if (Object.entries(whereOpc).length !== 0) {
            Object.keys(whereOpc).forEach(function (key, i) {
                if (i === 0) {
                    wheres.push(` ${tb}.${key} = '${whereOpc[key]}'`);
                }
                else {
                    wheres.push(`AND ${tb}.${key} = '${whereOpc[key]}'`);
                }
            });
            query.push(` WHERE ${wheres.join(' ')} `);
        }
        if (Object.entries(filters).length) {
            let set = [];
            Object.keys(filters).forEach(function (key, i) {
                if (isNaN(filters[key])) {
                    let item = `AND ${key} LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
                else {
                    let item = `AND CAST(${key} AS TEXT) LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
            });
            query.push(set.join(' '));
        }
        if (orderBy !== '') {
            query.push(` ORDER BY ${orderBy} `);
        }
        query.push(` LIMIT ${limit} OFFSET ((${page} - 1 ) * ${limit});`);
        return query.join(' ');
    }
    SQLCOUNTROWS(item, status, val, filters = {}, operator = "=") {
        const tb = this.table;
        let query = [`SELECT count(${item}) FROM ${tb} WHERE ${status} ${operator} ${val}`];
        if (Object.entries(filters).length) {
            let set = [];
            Object.keys(filters).forEach(function (key, i) {
                if (isNaN(filters[key])) {
                    let item = `AND ${key} LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
                else {
                    let item = `AND CAST(${key} AS TEXT) LIKE '%${filters[key]}%'`;
                    set.push(item);
                }
            });
            query.push(set.join(' '));
        }
        return query.join(' ');
    }
    SQLINSERTID() {
        const query = `SELECT currval(pg_get_serial_sequence('${this.table}','id'));`;
        return query;
    }
}
exports.default = ConstantQuerys;
//# sourceMappingURL=constant-query.js.map