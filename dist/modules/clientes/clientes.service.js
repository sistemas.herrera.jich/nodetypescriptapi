"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../database/config"));
const constant_query_1 = __importDefault(require("../../global/constant-query"));
const aws_config_1 = require("../../helpers/aws-config");
const constantQuery = new constant_query_1.default("clientes");
class ClienteService {
    constructor() {
        this.client = config_1.default;
    }
    getClientes(limit = 8, page, filters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let countRows = 0;
                let queryRows = constantQuery.SQLCOUNTROWS("id", "status", 1, filters);
                console.log(queryRows);
                const result = yield this.client.query(queryRows);
                const params = { 1: "fecha", 2: "folio", 3: "rfc", 4: "razon_social", 5: "id" }, innerJoins = {}, whereOpc = { status: 1 }, orderBy = 'clientes.fecha ASC';
                const query = constantQuery.SQLGET(limit, page, filters, params, innerJoins, whereOpc, orderBy);
                const rows = yield this.client.query(query);
                countRows = Math.ceil(result.rows[0].count / limit);
                const data = {
                    limit,
                    page,
                    numpages: countRows,
                    clientes: rows.rows
                };
                return data;
            }
            catch (error) {
                throw new Error("Error en la consulta" + error);
            }
        });
    }
    addPDFCif(file) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = file.id;
                let name = file.name;
                let buff = file.buff;
                let mimetype = file.mimetype;
                const datas3 = (0, aws_config_1.DATAS3)(`clientes/cif/${name}`, buff, mimetype);
                const { Location } = yield aws_config_1.s3.upload(datas3).promise();
                const data = {
                    pdf: Location
                };
                const query = constantQuery.SQLUPDATE(data, id);
                yield this.client.query(query);
                return "PDF Guardado correctamente";
            }
            catch (error) {
                throw new Error("Error en la consulta " + error);
            }
        });
    }
    createClientes(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (Object.entries(obj).length === 0)
                    throw new Error("El objeto users esta vacio");
                const rfc = obj.rfc;
                const query = constantQuery.SQLGETALL("rfc", rfc);
                const { rows } = this.client.query(query);
                if (rows.length === 0) {
                    return "Ya existe el cliente";
                }
                const insert = constantQuery.SQLINSERT(obj);
                yield this.client.query(insert);
                return "Clientes correctamente creado";
            }
            catch (error) {
                throw new Error("Error en consulta " + error);
            }
        });
    }
    getSaldoClientes(idAgente) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = `select clientes.idadminpaq as id, clientes.razon_social as cliente, clientes.rfc as rfc, users.nombre as agente,
            sum(documentos.pendiente) as saldo
            from clientes
            left join documentos on documentos.idcliente = clientes.idadminpaq
            inner join users on users.idadminpaq = clientes.agente_venta 
            where documentos.pendiente::numeric::int > 3 and users.id = ${idAgente}
            group by clientes.idadminpaq, clientes.razon_social, clientes.rfc, users.nombre
            order by saldo desc `;
                const saldos = yield this.client.query(query);
                return saldos.rows;
            }
            catch (error) {
                throw new Error("Error en la consulta" + error);
            }
        });
    }
    getSaldosAdmin() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = `select clientes.idadminpaq as id, clientes.razon_social as cliente, clientes.rfc as rfc,
            sum(documentos.pendiente) as pendiente
            from clientes
            left join documentos on documentos.idcliente = clientes.idadminpaq 
            where documentos.pendiente::numeric::int > 3
            group by clientes.idadminpaq, clientes.razon_social, clientes.rfc
            order by pendiente DESC `;
                const saldos = yield this.client.query(query);
                return saldos.rows;
            }
            catch (error) {
                throw new Error("Error en la consulta" + error);
            }
        });
    }
    getFacturasCliente(idCliente) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = `select clientes.idadminpaq as id, 
            clientes.razon_social as cliente, 
            clientes.rfc as rfc,
            documentos.pendiente as saldo,
            documentos.folio as folio,
            documentos.observaciones as observaciones
            from clientes
            inner join documentos on documentos.idcliente = clientes.idadminpaq
            where documentos.pendiente::numeric::int > 3 and clientes.idadminpaq = ${idCliente}
            group by documentos.folio, documentos.pendiente,clientes.idadminpaq, clientes.razon_social, clientes.rfc, documentos.observaciones
            order by saldo desc`;
                const facturas = yield this.client.query(query);
                return facturas.rows;
            }
            catch (error) {
                throw new Error("Error en " + error);
            }
        });
    }
}
exports.default = ClienteService;
//# sourceMappingURL=clientes.service.js.map