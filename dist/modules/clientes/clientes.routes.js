"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientesRoutes = void 0;
const express_1 = require("express");
const authenticated_jwt_1 = require("../../middlewares/authenticated-jwt");
const clientes_controller_1 = __importDefault(require("./clientes.controller"));
const router = (0, express_1.Router)();
class ClientesRoutes {
    constructor() {
        this.clientesController = new clientes_controller_1.default();
    }
    get routes() {
        let controller = this.clientesController;
        router.post('/all/:limit/:page', controller.getClientes);
        router.post('/create', controller.createClientes);
        router.put('/addPdf', controller.addPDFCif);
        router.get('/saldos/:id', [authenticated_jwt_1.authenticated_JWT], controller.getSaldos);
        router.get('/facturas/:id', [authenticated_jwt_1.authenticated_JWT], controller.getFacturas);
        router.get('/saldos_admin', [authenticated_jwt_1.authenticated_JWT], controller.getSaldosAdmin);
        return router;
    }
}
exports.ClientesRoutes = ClientesRoutes;
//# sourceMappingURL=clientes.routes.js.map