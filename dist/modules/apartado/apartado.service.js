"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../database/config"));
const users_constant_1 = require("../users/users.constant");
const constant_query_1 = __importDefault(require("../../global/constant-query"));
const constantQuery = new constant_query_1.default("apartados");
class ApartadosServices {
    constructor() {
        this.client = config_1.default;
    }
    create(apartado) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (Object.entries(apartado).length === 0)
                    throw new Error("El objeto apartado esta vacio");
                let query = constantQuery.SQLINSERT(apartado);
                yield this.client.query(query);
                let queryId = constantQuery.SQLINSERTID();
                const lastidrows = yield this.client.query(queryId);
                return lastidrows.rows;
            }
            catch (error) {
                throw new Error("Error al crear apartado: " + error);
            }
        });
    }
    getUsuario(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let query = (0, users_constant_1.SQLUSER)(id);
                const user = yield this.client.query(query);
                return user.rows[0];
            }
            catch (error) {
                throw new Error("Error: " + error);
            }
        });
    }
    getProducto(codigo) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let query = `select productos.codigo as codigo, productos.nombre as nombre, 
                            productos.existencia as pieza, productos.id as id, modelo.url as url
                            from productos 
                            left join modelo on modelo.id = productos.id_modelo
                            where productos.codigo ='${codigo}'`;
                const producto = yield this.client.query(query);
                return producto.rows[0];
            }
            catch (error) {
                throw new Error("Error: " + error);
            }
        });
    }
    getApartado(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let query = constantQuery.SQLGETBYONE('id', id);
                const apartado = yield this.client.query(query);
                return apartado.rows[0];
            }
            catch (error) {
                throw new Error("Error: " + error);
            }
        });
    }
}
exports.default = ApartadosServices;
//# sourceMappingURL=apartado.service.js.map