"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApartadoRoutes = void 0;
const express_1 = require("express");
const authenticated_jwt_1 = require("../../middlewares/authenticated-jwt");
const apartado_controller_1 = require("./apartado.controller");
const router = (0, express_1.Router)();
router.use(authenticated_jwt_1.authenticated_JWT);
class ApartadoRoutes {
    constructor() {
        this.apartado = new apartado_controller_1.ApartadoController();
    }
    get routes() {
        let controller = this.apartado;
        router.post('/create', controller.create);
        return router;
    }
}
exports.ApartadoRoutes = ApartadoRoutes;
//# sourceMappingURL=apartado.routes.js.map