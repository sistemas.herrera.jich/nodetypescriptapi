"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApartadoController = void 0;
const http_status_1 = __importDefault(require("http-status"));
const nodemailer_1 = __importDefault(require("nodemailer"));
const apartado_service_1 = __importDefault(require("./apartado.service"));
const emailsend_1 = require("../../helpers/emailsend");
let services = new apartado_service_1.default();
class ApartadoController {
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let obj = req.body;
                if (Object.entries(obj).length === 0) {
                    return res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: "Error no se puede agregar objeto vacio"
                    });
                }
                const insertId = yield services.create(obj);
                const apartado = yield services.getApartado(insertId[0].currval);
                const usuario = yield services.getUsuario(apartado.id_user);
                const producto = yield services.getProducto(apartado.codigo);
                const replacements = {
                    vendedor: usuario.nombre,
                    razonSocial: apartado.razon_social,
                    domicilio: apartado.direccion,
                    telefono: apartado.telefono,
                    notes: apartado.notes,
                    url: producto.url,
                    codigo: producto.codigo,
                    cantidad: apartado.piezas,
                    productoName: producto.nombre,
                    precio: apartado.precio
                };
                const htmlToSend = (0, emailsend_1.EMAIL)(replacements);
                const transporter = nodemailer_1.default.createTransport({
                    host: 'smtp.gmail.com',
                    services: 'gmail',
                    auth: {
                        user: 'noreply.jhkmultiservicios@jhkmservicios.com',
                        pass: 'Jmu150331lx7',
                    }
                });
                const info = yield transporter.sendMail({
                    from: apartado.tipo == "apartado"
                        ? ' "Solicitud de apartado" <noreply.jhkmultiservicios@jhkmservicios.com>'
                        : '"Cotización " <noreply.jhkmultiservicios@jhkmservicios.com>',
                    to: apartado.tipo == "apartado"
                        ? "hector.ceniceros@jhkmservicios.com"
                        : apartado.email,
                    cc: apartado.tipo == "apartado"
                        ? "auxiliar@jhkmservicios.com, alexccp11@gmail.com"
                        : "auxiliar@jhkmservicios.com, alexccp11@gmail.com, hector.ceniceros@jhkmservicios.com",
                    subject: `"Pedido ${replacements.razonSocial}"`,
                    text: `"Vendedor: ${replacements.vendedor}"`,
                    html: htmlToSend,
                });
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: `Correo enviado`,
                    data: info.response
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: 'Error al agregar apartado'
                });
            }
        });
    }
}
exports.ApartadoController = ApartadoController;
//# sourceMappingURL=apartado.controller.js.map