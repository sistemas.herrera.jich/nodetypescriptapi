"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../database/config"));
const promociones_constant_1 = __importDefault(require("./promociones.constant"));
const constantQuery = new promociones_constant_1.default("promociones");
class PromocionesService {
    constructor() {
        this.client = config_1.default;
    }
    getPromociones(limit = 8, page, filters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let countRows = 0;
                let queryRows = constantQuery.SQLCOUNTROWS("id", "status", 1, filters);
                const result = yield this.client.query(queryRows);
                const params = { 1: "url", 2: "descripcion", 3: "create_at", 4: "expiration_date", 5: "id", 6: "codigo" }, innerJoins = {}, whereOpc = { status: ` = 1`, expiration_date: ` > '${new Date(Date.now()).toISOString()}'` }, orderBy = 'promociones.expiration_date DESC';
                const query = constantQuery.SQLGET(limit, page, filters, params, innerJoins, whereOpc, orderBy);
                const rows = yield this.client.query(query);
                countRows = Math.ceil(result.rows[0].count / limit);
                const data = {
                    limit,
                    page,
                    numpages: countRows,
                    promociones: rows.rows
                };
                return data;
            }
            catch (error) {
                throw new Error("Error en la consulta" + error);
            }
        });
    }
}
exports.default = PromocionesService;
//# sourceMappingURL=promociones.service.js.map