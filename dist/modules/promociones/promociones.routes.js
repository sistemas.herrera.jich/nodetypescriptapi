"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PromocionesRoutes = void 0;
const express_1 = require("express");
const authenticated_jwt_1 = require("../../middlewares/authenticated-jwt");
const promociones_controller_1 = __importDefault(require("./promociones.controller"));
const router = (0, express_1.Router)();
router.use(authenticated_jwt_1.authenticated_JWT);
class PromocionesRoutes {
    constructor() {
        this.promociones = new promociones_controller_1.default();
    }
    get routes() {
        let controller = this.promociones;
        router.post('/all/:limit/:page', controller.getPromociones);
        return router;
    }
}
exports.PromocionesRoutes = PromocionesRoutes;
//# sourceMappingURL=promociones.routes.js.map