"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const promociones_service_1 = __importDefault(require("./promociones.service"));
const service = new promociones_service_1.default();
class PromocionesController {
    getPromociones(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const limit = parseInt(req.params.limit);
                const page = parseInt(req.params.page);
                const clientes = yield service.getPromociones(limit, page, body);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: "Listado Promociones",
                    data: clientes
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error en la consulta"
                });
            }
        });
    }
}
exports.default = PromocionesController;
//# sourceMappingURL=promociones.controller.js.map