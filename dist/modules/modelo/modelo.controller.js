"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const modelo_service_1 = __importDefault(require("./modelo.service"));
const services = new modelo_service_1.default();
class ModeloController {
    validateObj(obj) {
        if (Object.entries(obj).length === 0) {
            return true;
        }
        else {
            return false;
        }
    }
    createModel(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { modelo } = req.body;
                if (this.validateObj(modelo)) {
                    res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: "El modelo no puede ir vacio"
                    });
                }
                const msg = yield services.createModel(modelo);
                const dataModel = yield services.getModels();
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: msg,
                    data: dataModel
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al crear modelo ${error}`
                });
            }
        });
    }
    updateModel(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = parseInt(req.params.id);
                const { modelo } = req.body;
                if (this.validateObj(modelo)) {
                    res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: "El modelo no puede ir vacio"
                    });
                }
                const msg = yield services.updateModel(id, modelo);
                const modelos = yield services.getModels();
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: msg,
                    data: modelos
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al actualizar modelo ${error}`
                });
            }
        });
    }
    getModels(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const limit = parseInt(req.params.limit);
                const page = parseInt(req.params.page);
                const modelo = yield services.getModels();
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: 'Listado de modelos',
                    data: modelo
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al consultar modelo ${error}`
                });
            }
        });
    }
    getModelsTable(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const limit = parseInt(req.params.limit);
                const page = parseInt(req.params.page);
                const modelo = yield services.getModelsTable(limit, page, body);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: 'Listado de modelos',
                    data: modelo
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al consultar modelo ${error}`
                });
            }
        });
    }
    getModel(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.params.id;
                const model = yield services.getModel(id);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: 'Modelo encontrado',
                    data: model
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al consultar modelo ${error}`
                });
            }
        });
    }
    addImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id, filename } = req.body;
                const data = {
                    id,
                    name: `image_${id}`,
                    filename
                };
                const msg = yield services.uploadImage(data);
                const modelos = yield services.getModels();
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: msg,
                    data: modelos
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: `Error al subir imagen a modelo ${error}`
                });
            }
        });
    }
}
exports.default = ModeloController;
//# sourceMappingURL=modelo.controller.js.map