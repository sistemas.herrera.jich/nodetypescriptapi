"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ModeloRoutes = void 0;
const express_1 = require("express");
const authenticated_jwt_1 = require("../../middlewares/authenticated-jwt");
const modelo_controller_1 = __importDefault(require("./modelo.controller"));
const router = (0, express_1.Router)();
router.use(authenticated_jwt_1.authenticated_JWT);
class ModeloRoutes {
    constructor() {
        this.modeloController = new modelo_controller_1.default();
    }
    get routes() {
        let controller = this.modeloController;
        router.post('/create', controller.createModel);
        router.post('/getModelTable/:limit/:page', controller.getModelsTable);
        router.put('/update/:id', controller.updateModel);
        router.get('/getAll', controller.getModels);
        router.get('/getOne/:id', controller.getModel);
        router.post('/addImage', controller.addImage);
        return router;
    }
}
exports.ModeloRoutes = ModeloRoutes;
//# sourceMappingURL=modelo.routes.js.map