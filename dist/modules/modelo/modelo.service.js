"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../../database/config"));
const constant_query_1 = __importDefault(require("../../global/constant-query"));
const aws_config_1 = require("../../helpers/aws-config");
const base64_destructure_1 = require("../../helpers/base64-destructure");
const modelo_constant_1 = require("./modelo.constant");
const constantQuery = new constant_query_1.default("modelo");
class ModeloServices {
    constructor() {
        this.client = config_1.default;
    }
    createModel(model) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (Object.entries(model).length === 0)
                    throw new Error("Modelo no puede ir vacio");
                const insert = (0, modelo_constant_1.SQLINSERT)(model);
                yield this.client.query(insert);
                return "Modelo creado correctamente";
            }
            catch (error) {
                throw new Error("Error al crear modelo");
            }
        });
    }
    updateModel(id, model) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (Object.entries(model).length === 0)
                    throw new Error("Modelo no puede ir vacio");
                const update = (0, modelo_constant_1.SQLUPDATE)(id, model);
                yield this.client.query(update);
                return "Modelo actulizado correctamente";
            }
            catch (error) {
                throw new Error("Error al actulizar modelo");
            }
        });
    }
    getModelsTable(limit = 8, page, filters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let countRows = 0;
                let queryRows = constantQuery.SQLCOUNTROWS("id", "status", 1, filters);
                const result = yield this.client.query(queryRows);
                const params = { 1: "nombre", 2: "url", 3: "id" }, innerJoins = {}, whereOpc = { status: 1 }, orderBy = 'modelo.nombre ASC';
                const query = constantQuery.SQLGET(limit, page, filters, params, innerJoins, whereOpc, orderBy);
                const rows = yield this.client.query(query);
                countRows = Math.ceil(result.rows[0].count / limit);
                const data = {
                    limit,
                    page,
                    numpages: countRows,
                    data: rows.rows
                };
                return data;
            }
            catch (error) {
                throw new Error("Error al traer los modelos");
            }
        });
    }
    getModels() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = (0, modelo_constant_1.SQLMODELOS)();
                const { rows } = yield this.client.query(query);
                return rows;
            }
            catch (error) {
                throw new Error("Error al traer los modelos");
            }
        });
    }
    getModel(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = (0, modelo_constant_1.SQLMODELO)(id);
                const { rows } = yield this.client.query(query);
                return rows[0];
            }
            catch (error) {
                throw new Error("Error al traer los modelos");
            }
        });
    }
    uploadImage(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = obj.id;
                let name = obj.name;
                let filename = obj.filename;
                const b64clean = (0, base64_destructure_1.limpiar)(filename);
                const datas3 = (0, aws_config_1.DATAS3)(`productos/${name}.${b64clean.type}`, b64clean.buff, b64clean.mimetype);
                let location = '';
                const { Location } = yield aws_config_1.s3.upload(datas3).promise();
                location = Location;
                const data = {
                    url: location
                };
                const query = (0, modelo_constant_1.SQLUPDATE)(id, data);
                yield this.client.query(query);
                return "Imagen agregada al modelo con exito";
            }
            catch (error) {
                throw new Error("Imagen no se guardo correctamente" + error);
            }
        });
    }
}
exports.default = ModeloServices;
//# sourceMappingURL=modelo.service.js.map