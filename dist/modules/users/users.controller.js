"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_service_1 = __importDefault(require("./users.service"));
const http_status_1 = __importDefault(require("http-status"));
let services = new users_service_1.default();
class UserController {
    createUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let obj = req.body;
                if (Object.entries(obj).length === 0) {
                    return res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: "Error no se puede agregar objeto vacio"
                    });
                }
                const message = yield services.createUsers(obj);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: message,
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se pudo completar consulta"
                });
            }
        });
    }
    updateUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = parseInt(req.params.id);
                let obj = req.body;
                const msg = yield services.updateUsers(id, obj);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: msg
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se pudo completar consulta"
                });
            }
        });
    }
    getUsers(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const limit = parseInt(req.params.limit);
                const page = parseInt(req.params.page);
                const users = yield services.getUsers(limit, page, body);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: 'Listado de usuarios',
                    data: users
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se pudo completar consulta"
                });
            }
        });
    }
    getUser(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = parseInt(req.params.id);
                const user = yield services.getUser(id);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: 'Usuario encontrado',
                    data: user
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se pudo completar consulta"
                });
            }
        });
    }
    addImage(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id, filename } = req.body;
                const data = {
                    id,
                    name: `image_${id}`,
                    filename
                };
                const msg = yield services.uploadImage(data);
                const user = yield services.getUser(id);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: msg,
                    data: user
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error no se pudo subir la imagen" + error
                });
            }
        });
    }
}
exports.default = UserController;
//# sourceMappingURL=users.controller.js.map