"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const productos_service_1 = __importDefault(require("./productos.service"));
const service = new productos_service_1.default();
class ProductoController {
    getProductos(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let limit = parseInt(req.params.limit);
                let page = parseInt(req.params.page);
                let filters = req.body;
                const productos = yield service.getProductos(limit, page, filters);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: "Listado de productos",
                    data: productos
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error en la consulta de productos"
                });
            }
        });
    }
}
exports.default = ProductoController;
//# sourceMappingURL=productos.controller.js.map