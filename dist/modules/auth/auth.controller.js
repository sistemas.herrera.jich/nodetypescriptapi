"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const http_status_1 = __importDefault(require("http-status"));
const users_service_1 = __importDefault(require("../users/users.service"));
const jwt_1 = require("./jwt");
let services = new users_service_1.default();
class AuthController {
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email, password } = req.body;
                const user = yield services.login(email);
                if (user.id) {
                    const pwd = bcryptjs_1.default.compareSync(password, user.password);
                    if (!pwd) {
                        return res.status(http_status_1.default.BAD_REQUEST).json({
                            isValid: false,
                            message: `Credenciales incorrectas`
                        });
                    }
                    const token = yield (0, jwt_1.generate_jwt)(user);
                    res.status(http_status_1.default.OK).json({
                        isValid: true,
                        message: `Login correcto`,
                        data: user,
                        token: token
                    });
                }
                else {
                    res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: `Credenciales incorrectas`
                    });
                }
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: ` Credenciales incorrectas`
                });
            }
        });
    }
    verify(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { sub } = req.body;
                const user = yield services.getUser(sub);
                const token = yield (0, jwt_1.generate_jwt)(user);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: `Verificacion de token correcta`,
                    data: user,
                    token: token
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: error
                });
            }
        });
    }
}
exports.default = AuthController;
//# sourceMappingURL=auth.controller.js.map