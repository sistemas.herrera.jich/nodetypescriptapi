"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClientesRoutes = void 0;
const express_1 = require("express");
const clientes_controller_1 = __importDefault(require("./clientes.controller"));
const router = (0, express_1.Router)();
class ClientesRoutes {
    constructor() {
        this.clientesController = new clientes_controller_1.default();
    }
    get routes() {
        let controller = this.clientesController;
        router.post('/all/:limit/:page', controller.getClientes);
        router.post('/create', controller.createClientes);
        router.put('/addPdf', controller.addPDFCif);
        return router;
    }
}
exports.ClientesRoutes = ClientesRoutes;
//# sourceMappingURL=clientes.routes.js.map