"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_1 = __importDefault(require("http-status"));
const clientes_services_1 = __importDefault(require("./clientes.services"));
const service = new clientes_services_1.default();
class ClientesController {
    getClientes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const limit = parseInt(req.params.limit);
                const page = parseInt(req.params.page);
                const clientes = yield service.getClientes(limit, page, body);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: "Listado Clientes",
                    data: clientes
                });
            }
            catch (error) {
                console.log(error);
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error en la consulta"
                });
            }
        });
    }
    addPDFCif(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const id = req.body.id;
                const pdf = req.body.pdf;
                const dataimg = pdf.split(';');
                const mimetype = dataimg[0].split(':');
                const type = mimetype[1].split('/');
                const base64 = dataimg[1].split(',');
                const buff = Buffer.from(base64[1], 'base64');
                const obj = {
                    id: id,
                    name: `cif_pdf_${id}.${type[1]}`,
                    buff: buff,
                    mimetype: mimetype
                };
                const responce = yield service.addPDFCif(obj);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: responce
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: error
                });
            }
        });
    }
    createClientes(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { razon_social, rfc, pdf } = req.body;
                const obj = {
                    razon_social,
                    rfc
                };
                if (Object.entries(obj).length === 0) {
                    return res.status(http_status_1.default.BAD_REQUEST).json({
                        isValid: false,
                        message: "Error no se puede agregar objeto vacio"
                    });
                }
                const responce = yield service.createClientes(obj);
                const data = yield service.getClientes(1, 1, { "rfc": rfc });
                const dataimg = pdf.split(';');
                const mimetype = dataimg[0].split(':');
                const type = mimetype[1].split('/');
                const base64 = dataimg[1].split(',');
                const buff = Buffer.from(base64[1], 'base64');
                const id = data.clientes.id;
                const file = {
                    id: id,
                    name: `cif_pdf_${id}.${type[1]}`,
                    buff: buff,
                    mimetype: mimetype
                };
                const message = yield service.addPDFCif(file);
                res.status(http_status_1.default.OK).json({
                    isValid: true,
                    message: `Alta: ${responce}, PDF: ${message}`,
                    data: data
                });
            }
            catch (error) {
                res.status(http_status_1.default.BAD_REQUEST).json({
                    isValid: false,
                    message: "Error al agregar cliente"
                });
            }
        });
    }
}
exports.default = ClientesController;
//# sourceMappingURL=clientes.controller.js.map