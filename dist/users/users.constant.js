"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SQLLOGIN = exports.SQLINSERTUSERS = exports.SQLUPDATEUSERS = exports.SQLUSERBYEMAIL = exports.SQLUSER = exports.SQLUSERS = void 0;
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const SQLUSERS = () => {
    const query = `SELECT * FROM users WHERE status=1`;
    return query;
};
exports.SQLUSERS = SQLUSERS;
const SQLUSER = (id) => {
    const query = `SELECT * FROM users WHERE status=1 AND id=${id}`;
    return query;
};
exports.SQLUSER = SQLUSER;
const SQLUSERBYEMAIL = (email) => {
    const query = `SELECT id FROM users WHERE email='${email}'`;
    return query;
};
exports.SQLUSERBYEMAIL = SQLUSERBYEMAIL;
const SQLUPDATEUSERS = (id, obj) => {
    let query = ['UPDATE users SET'];
    let set = [];
    Object.keys(obj).forEach(function (key, i) {
        if (key === 'email') {
        }
        else if (key === 'password') {
            let salt = bcryptjs_1.default.genSaltSync();
            let password = bcryptjs_1.default.hashSync(obj[key], salt);
            let item = `${key}='${password}'`;
            set.push(item);
        }
        else {
            let item = `${key}='${obj[key]}'`;
            set.push(item);
        }
    });
    query.push(set.join(', '));
    query.push(`WHERE id=${id}`);
    return query.join(' ');
};
exports.SQLUPDATEUSERS = SQLUPDATEUSERS;
const SQLINSERTUSERS = (obj) => {
    let query = ['INSERT INTO users'];
    let cols = [];
    Object.keys(obj).forEach(function (key, i) {
        cols.push(key);
    });
    query.push(`( ${cols.join(', ')} ) VALUES`);
    let values = [];
    Object.keys(obj).forEach(function (key, i) {
        if (key === 'password') {
            let salt = bcryptjs_1.default.genSaltSync();
            let password = bcryptjs_1.default.hashSync(obj[key], salt);
            values.push(`'${password}'`);
        }
        else {
            values.push(`'${obj[key]}'`);
        }
    });
    query.push(`( ${values.join(', ')} )`);
    return query.join(' ');
};
exports.SQLINSERTUSERS = SQLINSERTUSERS;
const SQLLOGIN = (email) => {
    const query = `SELECT * FROM users WHERE email='${email}' AND status=1`;
    return query;
};
exports.SQLLOGIN = SQLLOGIN;
//# sourceMappingURL=users.constant.js.map