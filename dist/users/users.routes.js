"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersRoutes = void 0;
const express_1 = require("express");
const users_controller_1 = __importDefault(require("./users.controller"));
const authenticated_jwt_1 = require("../middlewares/authenticated-jwt");
const router = (0, express_1.Router)();
router.use(authenticated_jwt_1.authenticated_JWT);
class UsersRoutes {
    constructor() {
        this.usersController = new users_controller_1.default();
    }
    get routes() {
        let controller = this.usersController;
        router.post('/create', controller.createUsers);
        router.put('/update/:id', controller.updateUsers);
        router.post('/getAll/:limit/:page', controller.getUsers);
        router.get('/getUser/:id', controller.getUser);
        router.post('/addImage', controller.addImage);
        return router;
    }
}
exports.UsersRoutes = UsersRoutes;
//# sourceMappingURL=users.routes.js.map