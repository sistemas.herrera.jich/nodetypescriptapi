"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../database/config"));
const aws_config_1 = require("../helpers/aws-config");
const users_constant_1 = require("./users.constant");
const constant_query_1 = __importDefault(require("../global/constant-query"));
const constantQuery = new constant_query_1.default("users");
class UsersServices {
    constructor() {
        this.client = config_1.default;
    }
    createUsers(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (Object.entries(obj).length === 0)
                    throw new Error("El objeto users esta vacio");
                let query = (0, users_constant_1.SQLUSERBYEMAIL)(obj.email);
                const { rows } = yield this.client.query(query);
                if (rows.length !== 0) {
                    return "El usuario ya existe";
                }
                let insert = (0, users_constant_1.SQLINSERTUSERS)(obj);
                yield this.client.query(insert);
                return "Creado correctamente";
            }
            catch (error) {
                throw new Error("Error al crear usuarios: " + error);
            }
        });
    }
    updateUsers(id, obj) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (isNaN(id)) {
                    return "El usuario no existe";
                }
                let update = (0, users_constant_1.SQLUPDATEUSERS)(id, obj);
                yield this.client.query(update);
                return "Actulizado correctamente";
            }
            catch (error) {
                throw new Error("Error al actulizar usuarios: " + error);
            }
        });
    }
    getUsers(limit = 8, page, filters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let countRows = 0;
                let queryRows = constantQuery.SQLCOUNTROWS("id", "status", 1, filters);
                const result = yield this.client.query(queryRows);
                const params = {}, innerJoins = {}, whereOpc = { status: 1 }, orderBy = 'users.id ASC';
                const query = constantQuery.SQLGET(limit, page, filters, params, innerJoins, whereOpc, orderBy);
                const rows = yield this.client.query(query);
                countRows = Math.ceil(result.rows[0].count / limit);
                const data = {
                    limit,
                    page,
                    numpages: countRows,
                    clientes: rows.rows
                };
                return data;
            }
            catch (error) {
                throw new Error("Error al consultar usuarios: " + error);
            }
        });
    }
    getUser(id) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = (0, users_constant_1.SQLUSER)(id);
                const { rows } = yield this.client.query(query);
                return rows[0];
            }
            catch (error) {
                throw new Error("Error al consultar usuarios: " + error);
            }
        });
    }
    login(email) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const query = (0, users_constant_1.SQLLOGIN)(email);
                const { rows } = yield this.client.query(query);
                return rows[0];
            }
            catch (error) {
                throw new Error("Error al consultar usuarios: " + error);
            }
        });
    }
    uploadImage(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let id = obj.id;
                let name = obj.name;
                let filename = obj.filename;
                const datas3 = (0, aws_config_1.DATAS3)(`usuarios/${name}`, filename);
                let location = '';
                const { Location } = yield aws_config_1.s3.upload(datas3).promise();
                location = Location;
                const data = {
                    imagen: location
                };
                const query = (0, users_constant_1.SQLUPDATEUSERS)(id, data);
                yield this.client.query(query);
                return "Imagen agregada al modelo con exito";
            }
            catch (error) {
                console.log(error);
                throw new Error("Error no se pudo guardar imagen");
            }
        });
    }
}
exports.default = UsersServices;
//# sourceMappingURL=users.services.js.map