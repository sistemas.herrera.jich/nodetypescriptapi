"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SQLROWSPRODUCTOSCOUNT = exports.SQLPRODUCTOS = exports.SQLUPDATE = exports.SQLINSERT = void 0;
const SQLINSERT = (obj) => {
    let query = ['INSERT INTO productos'];
    let cols = [];
    Object.keys(obj).forEach(function (key, i) {
        cols.push(key);
    });
    query.push(`( ${cols.join(', ')} ) VALUES`);
    let values = [];
    Object.keys(obj).forEach(function (key, i) {
        values.push(`'${obj[key]}'`);
    });
    query.push(`( ${values.join(', ')} )`);
    return query.join(' ');
};
exports.SQLINSERT = SQLINSERT;
const SQLUPDATE = (id, obj) => {
    let query = ['UPDATE productos SET'];
    let set = [];
    Object.keys(obj).forEach(function (key, i) {
        let item = `${key}='${obj[key]}'`;
        set.push(item);
    });
    query.push(set.join(', '));
    query.push(`WHERE id=${id}`);
    return query.join(' ');
};
exports.SQLUPDATE = SQLUPDATE;
const SQLPRODUCTOS = (limit, page, filters) => {
    let query = '';
    if (Object.entries(filters).length === 0) {
        query = `SELECT 
                    productos.*,
                    modelo.nombre as nombre_modelo,
                    modelo.url as imagen_producto
                FROM  productos
                LEFT JOIN modelo ON productos.id_modelo = modelo.id
                WHERE productos.existencia !=0
                ORDER BY productos.existencia DESC 
                LIMIT ${limit}
                OFFSET ((${page} - 1 ) * ${limit})`;
    }
    else {
        query = filtersConstructor(filters, limit, page);
    }
    return query;
};
exports.SQLPRODUCTOS = SQLPRODUCTOS;
const SQLROWSPRODUCTOSCOUNT = () => {
    let query = `SELECT count(id) from productos WHERE existencia != 0`;
    return query;
};
exports.SQLROWSPRODUCTOSCOUNT = SQLROWSPRODUCTOSCOUNT;
function filtersConstructor(filters, limit, page) {
    let query = [`SELECT productos.*,modelo.nombre as nombre_modelo,modelo.url as imagen_producto FROM  productos LEFT JOIN modelo ON productos.id_modelo = modelo.id WHERE productos.existencia !=0`];
    let set = [];
    Object.keys(filters).forEach(function (key, i) {
        let item = `AND productos.${key} LIKE '%${filters[key]}%'`;
        set.push(item);
    });
    query.push(set.join(' '));
    query.push(`ORDER BY productos.existencia LIMIT ${limit} OFFSET ((${page} - 1 ) * ${limit});`);
    return query.join(' ');
}
//# sourceMappingURL=productos.constant.js.map