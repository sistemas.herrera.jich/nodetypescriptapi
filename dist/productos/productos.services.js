"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("../database/config"));
const productos_constant_1 = require("./productos.constant");
const constant_query_1 = __importDefault(require("../global/constant-query"));
const constantQuery = new constant_query_1.default("productos");
class ProductoServices {
    constructor() {
        this.client = config_1.default;
    }
    getProductos(limit = 8, page, filters) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let countRows = 0;
                let queryRows = constantQuery.SQLCOUNTROWS("id", "existencia", 0, filters, "!=");
                const result = yield this.client.query(queryRows);
                let query = (0, productos_constant_1.SQLPRODUCTOS)(limit, page, filters);
                const rows = yield this.client.query(query);
                countRows = Math.ceil(result.rows[0].count / limit);
                let data = {
                    page: page,
                    limit: limit,
                    numpages: countRows,
                    data: rows.rows
                };
                return data;
            }
            catch (error) {
                throw new Error("Error al consultar productos: " + error);
            }
        });
    }
}
exports.default = ProductoServices;
//# sourceMappingURL=productos.services.js.map