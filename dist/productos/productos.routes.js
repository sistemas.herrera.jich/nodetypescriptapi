"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductosRoutes = void 0;
const express_1 = require("express");
const authenticated_jwt_1 = require("../middlewares/authenticated-jwt");
const productos_controller_1 = __importDefault(require("./productos.controller"));
const router = (0, express_1.Router)();
router.use(authenticated_jwt_1.authenticated_JWT);
class ProductosRoutes {
    constructor() {
        this.productosController = new productos_controller_1.default();
    }
    get routes() {
        let controller = this.productosController;
        router.post('/all/:limit/:page', controller.getProductos);
        return router;
    }
}
exports.ProductosRoutes = ProductosRoutes;
//# sourceMappingURL=productos.routes.js.map