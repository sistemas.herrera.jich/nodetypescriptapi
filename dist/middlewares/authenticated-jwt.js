"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verification_JWT = exports.authenticated_JWT = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const http_status_1 = __importDefault(require("http-status"));
const authenticated_JWT = (req, res, next) => {
    try {
        let token = req.headers['authorization'];
        if (typeof token === 'undefined') {
            return res.status(http_status_1.default.BAD_REQUEST).json({
                isValid: false,
                message: 'Invalid token undefined'
            });
        }
        let tokenComparation = token.split(' ');
        token = tokenComparation[1];
        let secret = process.env.SECRET_KEY || '';
        const { sub } = jsonwebtoken_1.default.verify(token, secret);
        next();
    }
    catch (error) {
        return res.status(http_status_1.default.BAD_REQUEST).json({
            isValid: false,
            message: 'Invalid token unauthorized ' + error
        });
    }
};
exports.authenticated_JWT = authenticated_JWT;
const verification_JWT = (req, res, next) => {
    try {
        let token = req.headers['authorization'];
        if (typeof token === 'undefined') {
            return res.status(http_status_1.default.BAD_REQUEST).json({
                isValid: false,
                message: 'Invalid token undefined'
            });
        }
        let tokenComparation = token.split(' ');
        token = tokenComparation[1];
        let secret = process.env.SECRET_KEY || '';
        const { sub } = jsonwebtoken_1.default.verify(token, secret);
        req.body = { sub };
        next();
    }
    catch (error) {
        return res.status(http_status_1.default.BAD_REQUEST).json({
            isValid: false,
            message: 'Invalid token unauthorized ' + error
        });
    }
};
exports.verification_JWT = verification_JWT;
//# sourceMappingURL=authenticated-jwt.js.map