"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const server_1 = __importDefault(require("./server"));
const port = parseInt(process.env.PORT || '');
const started = new server_1.default().start(port)
    .then(port => console.log(`Runing on port ${port}`))
    .catch(err => console.error(err));
exports.default = started;
//# sourceMappingURL=app.js.map