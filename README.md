# Project REST API NODEJS WITH TYPESCRIPT

Node JS

TypeScript

PostgreSQL

AWS-S3 Bucket

Json Web Token Security

## Table contents

* [Dependencies](#necesary-dependencies)
* [General Info](#general-info)
* [Setup](#setup)
* [Important](#important)

## Necesary Dependencies

### devDependencies

    "@types/bcryptjs"
    "@types/express"
    "@types/jsonwebtoken"
    "@types/node"
    "@types/pg"
    "nodemon"
    "tslint"
    "typescript"

### dependencies

    "aws-sdk"
    "bcryptjs"
    "cors"
    "dotenv"
    "express"
    "http-status"
    "jsonwebtoken"
    "moment"
    "pg"

## General info

* Project in node js for control of sales system in mobile app and web administrative system control.

* This project works with typescript for greater data security, with null safety technology standards.

## Setup

To run this project, install it locally using npm
<!-- markdownlint-disable MD046 -->
```text
npm install
npm run start:dev
```

## Important

create a env file with enviroment variables

```text
    HOST=YOURDBHOST
    USERDB=postgres
    PASSWORD=YOURPASSWORD
    DATABASE=YOURDBNAME
    DBPORT=5432
    MAX=5

    PORT=3005
    SECRET_KEY=YOURSECRETKEY

    S3USERS=YOURS3USERS
    S3IDCLAVEACCESO=YOURS3IDCLAVEACCESO
    S3CLAVEACCESO=YOURS3CLAVEACCESO
    S3BUCKEDNAME=YOURS3BUCKET
    S3REGION=YOURS3REGION
```
